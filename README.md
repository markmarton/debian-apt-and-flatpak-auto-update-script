#Update Script README.md

This is a simple update script for Ubuntu and other Debian-based systems that updates your system, cleans up unnecessary files, and updates your flatpak applications. The script also sends a notification to the user when the update is complete.
##Requirements

    Ubuntu or another Debian-based Linux distribution
    Flatpak installed (optional)

##Installation

    Save the script as update_script.sh in your home directory:

##shell

$ touch ~/update_script.sh
$ chmod +x ~/update_script.sh

    Open the file with your preferred text editor and paste the script content.

    Add the script to your crontab to run at startup (optional):

##bash

$ crontab -e

Add this line to your crontab:

bash

@reboot sleep 30 && /bin/bash /home/$USER/update_script.sh

Save and exit the file.
##Usage

To run the script manually:

bash

$ ./update_script.sh

If you added the script to your crontab, it will run automatically at system startup.
##Script overview

    Get the current logged-in X user.
    Run apt-get commands to update, upgrade, autoclean, and autoremove.
    Update flatpak applications (if installed).
    Send a notification to the user with the update status.

##Troubleshooting

If you encounter any issues running the script, check the following:

    Ensure the script has the correct permissions to execute (chmod +x update_script.sh).
    Make sure you are using a Debian-based Linux distribution (like Ubuntu).
    Verify that Flatpak is installed if you are using flatpak applications. If not, comment out or remove the flatpak update line from the script.
